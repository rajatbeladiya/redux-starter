import { AppRegistry } from "react-native";
import React from "react";
import { name as appName } from './app.json';
import { Provider } from "react-redux";
import App from './src/App';
import { createStore } from "redux";
import reducer from "./src/Store/reducer/mainReducer";

const store = createStore(reducer);

const AppContainer = () => (
    <Provider store={store}>
        <App />
    </Provider>
);

AppRegistry.registerComponent(appName, () => AppContainer);
